﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    [SerializeField] private GameObject m_restartPanel;

    public static UIManager Instance;

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        ShowPanel(false);
    }
    

    public void ShowPanel(bool value)
    {
        m_restartPanel.SetActive(value);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraActions : MonoBehaviour
{
    public Transform m_cameraTarget;

    public Vector3 m_cameraOffset;
    public float m_cameraSpeed;

    private Vector3 currentRotation;
    private Vector3 anglesToRotate;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        FollowPlayer();
        Rotation();
    }


    private void FollowPlayer()
    {
        Vector3 addOffset = m_cameraTarget.position + m_cameraOffset;
        Vector3 LerpPosition = Vector3.Lerp(transform.position, addOffset, m_cameraSpeed * Time.fixedDeltaTime);
        transform.position = LerpPosition;

        

        
    }

    public void Rotation()
    {

        transform.RotateAround(m_cameraTarget.position, Vector3.up, Input.GetAxis("Horizontal") * 3 * Time.deltaTime);
        //Quaternion rotationY = Quaternion.AngleAxis(m_cameraTarget.transform.rotation.y, new Vector3(0f, 1f, 0f));
        ////Quaternion rotationX = Quaternion.AngleAxis(anglesToRotate.x, new Vector3(1f, 0f, 0f));
        ////Quaternion rotationZ = Quaternion.AngleAxis(anglesToRotate.z, new Vector3(0f, 0f, 1f));
        //this.transform.rotation = this.transform.rotation * rotationY; //* rotationX * rotationZ;

        //currentRotation = currentRotation + anglesToRotate * Time.deltaTime;
        //currentRotation = new Vector3(currentRotation.x % 360f, currentRotation.y % 360f, currentRotation.z % 360f);
    }
}

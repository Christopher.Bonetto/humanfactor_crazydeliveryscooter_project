﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum VehicleState
{
    Ground,
    Air,
    Trick,
    NoInput
}

public class VehicleActions : MonoBehaviour
{
    #region Movement
    [SerializeField] private float m_acceleration;

    //[Space, Header("Veicle wheels stats")]
    //[SerializeField] private float wheelsForce;
    //[SerializeField] private float wheelsDistance;
    //[SerializeField] private Transform[] wheels;

    private Vector3 m_startingPosition;
    private float m_horizontalValue;
    private float m_verticalValue;
    [SerializeField]private float m_detectGroundDistance;

    private Rigidbody m_rb;
    #endregion

    #region Detection Area To Delivery

    public ICanDetect m_playerDetectInterface { get; private set; }

    private Collider[] m_playerDetectedColliders = new Collider[1];
    private int m_numberOfColliderDetected;

    [Range(0f, 360), SerializeField]
    private float m_detectDeliveryAngle;

    public float DetectDeliveryAngle
    {
        get
        {
            return Mathf.Clamp(m_detectDeliveryAngle, 0, 360);
        }
        private set
        {
            m_detectDeliveryAngle = value;
        }
    }

    [Range(0f, 30), SerializeField]
    private float m_detectDeliveryRadius;

    public float DetectDeliveryRadius
    {
        get
        {
            return Mathf.Clamp(m_detectDeliveryAngle, 0, 30);
        }
        private set
        {
            m_detectDeliveryAngle = value;
        }
    }

    [SerializeField] private LayerMask m_layerToDeliveryPizza;


    

    #region Detect pizza
    [Range(0f, 30), SerializeField]
    private float m_checkPizzaViewRadius;

    public float CheckPizzaViewRadius
    {
        get
        {
            return Mathf.Clamp(m_checkPizzaViewRadius, 0, 30);
        }
        private set
        {
            m_checkPizzaViewRadius = value;
        }
    }

    [SerializeField] private LayerMask m_layerToCheckPizza;
    #endregion

    #endregion

    #region Rotation

    [SerializeField] private float m_groundRotationSpeed = 2f;
    [SerializeField] private float m_trickRotationSpeed = 4f;

    private Quaternion m_startGameRotation;
    private float m_timer;
    public float m_checkTrickTimer;

    private Vector3 currentRotation;
    private Vector3 anglesToRotate;
    #endregion

    public VehicleState m_currentVehicleState { get; private set; } = VehicleState.Ground;

    public PizzaActions m_pizzaRef;

    private void Awake()
    {
        m_rb = gameObject.GetComponent<Rigidbody>();
    }

    private void Start()
    {
        m_playerDetectInterface = new DetectionTypes();

        m_startGameRotation = transform.rotation;
        m_startingPosition = transform.position;
    }


    private void Update()
    {
        if(m_pizzaRef != null)
        {
            DeliveryPizza();
        }
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        m_horizontalValue = Input.GetAxisRaw("Horizontal");
        m_verticalValue = Input.GetAxisRaw("Vertical");

        StateSystem();


        if (Input.GetKeyDown(KeyCode.Q))
        {
            Restart();
        }
    }

    public void StateSystem()
    {
        switch (m_currentVehicleState)
        {
            case VehicleState.Ground:
                {
                    if (!CheckGround())
                    {
                        ResetXRotation();
                        m_currentVehicleState = VehicleState.Air;
                    }
                    
                    MoveVeicle();
                    RotationWithDegree(m_horizontalValue * m_groundRotationSpeed);
                    if (Timer(0.3f))
                    {
                        DetectPizza();
                    }
                    break;
                }
            case VehicleState.Air:
                {
                    if (CheckGround()) m_currentVehicleState = VehicleState.Ground;

                    if (Timer(m_checkTrickTimer))
                    {
                        if (m_horizontalValue != 0 || m_verticalValue != 0)
                        {
                            m_currentVehicleState = VehicleState.Trick;
                        }
                    }
                    break;
                }
            case VehicleState.Trick:
                {
                    if (CheckGround())
                    {
                        ResetXRotation();
                        m_currentVehicleState = VehicleState.Ground;
                    }
                    
                    RotationWithDegree(m_horizontalValue * m_trickRotationSpeed, m_verticalValue * m_trickRotationSpeed);
                    
                    break;
                }
            case VehicleState.NoInput:
                {               
                    
                    if(m_rb.velocity == Vector3.zero)
                    {
                        m_currentVehicleState = VehicleState.Ground;
                    }
                    break;
                }
        }
    }

    public void Restart()
    {
        m_rb.velocity = Vector3.zero;        
        transform.rotation = m_startGameRotation;
        transform.position = m_startingPosition;
        m_currentVehicleState = VehicleState.Ground;
    }

    public void ResetXRotation()
    {
        Quaternion maintainRotationY = Quaternion.AngleAxis(transform.localEulerAngles.y, new Vector3(0f, 1f, 0f));
        Quaternion newRotationX = Quaternion.AngleAxis(0, new Vector3(1f, 0f, 0f));
        this.transform.rotation = newRotationX * maintainRotationY;
         
    }


    public bool CheckGround()
    {
        Debug.DrawLine(transform.position, -transform.up * m_detectGroundDistance, Color.red);
        // Check if there is ground
        if (Physics.Raycast(transform.position, -transform.up, m_detectGroundDistance))
        {
            m_rb.drag = 2;
            return true;
        }
        else
        {
            m_rb.drag = 0;
            return false;
        }

    }

    public void MoveVeicle()
    {
        //calculate forward force
        Vector3 forwardForce = transform.forward * m_acceleration * m_verticalValue;

        //correct force with mass and time
        forwardForce = forwardForce * Time.fixedDeltaTime * m_rb.mass;

        m_rb.AddForce(forwardForce);
    }


    public void RotationWithDegree(float yAxis = 0, float xAxis = 0, float zAxis = 0)
    {
        anglesToRotate.y = yAxis;
        anglesToRotate.x = xAxis;
        anglesToRotate.z = zAxis;

        Quaternion rotationY = Quaternion.AngleAxis(anglesToRotate.y, new Vector3(0f, 1f, 0f));
        Quaternion rotationX = Quaternion.AngleAxis(anglesToRotate.x, new Vector3(1f, 0f, 0f));
        Quaternion rotationZ = Quaternion.AngleAxis(anglesToRotate.z, new Vector3(0f, 0f, 1f));
        this.transform.rotation = this.transform.rotation * rotationY * rotationX * rotationZ;

        currentRotation = currentRotation + anglesToRotate * Time.deltaTime;
        currentRotation = new Vector3(currentRotation.x % 360f, currentRotation.y % 360f, currentRotation.z % 360f);
    }


    //public void ApplyForceToWheels()
    //{
    //    RaycastHit hit;

    //    foreach (Transform wheel in wheels)
    //    {
    //        Vector3 downForce;
    //        float distancePerc;

    //        if (Physics.Raycast(wheel.position, transform.up * -1, out hit, wheelsDistance))
    //        {
    //            distancePerc = 1 - (hit.distance / wheelsDistance);

    //            downForce = transform.up * wheelsForce * distancePerc;

    //            downForce = downForce * Time.deltaTime * m_rb.mass;

    //            m_rb.AddForceAtPosition(downForce, wheel.position);
    //        }
    //    }
    //}

    public bool Timer(float destinationTime)
    {
        m_timer += Time.deltaTime;

        if (m_timer >= destinationTime)
        {
            m_timer = 0;
            return true;
        }
        else
        {
            return false;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(m_currentVehicleState == VehicleState.Trick)
        {
            if(m_pizzaRef != null)
            {
                m_pizzaRef.DropPizza();
                m_pizzaRef = null;
            }
            m_currentVehicleState = VehicleState.NoInput;
        }
    }

    #region Delivery pizza
    private void DeliveryPizza()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (m_playerDetectInterface.DetectAreaWithAngle(gameObject.transform, m_detectDeliveryRadius, m_detectDeliveryAngle, m_playerDetectedColliders, m_numberOfColliderDetected, m_layerToDeliveryPizza) != null)
            {
                GameObject pizzaStation;
                pizzaStation = m_playerDetectInterface.DetectAreaWithAngle(gameObject.transform, m_detectDeliveryRadius, m_detectDeliveryAngle, m_playerDetectedColliders, m_numberOfColliderDetected, m_layerToDeliveryPizza);

                if (pizzaStation != null)
                {
                    m_pizzaRef.DeliveryThisPizza();
                    m_pizzaRef = null;
                }
            }
        }
    }

    public void DetectPizza()
    {
        GameObject pizza;

        if(m_playerDetectInterface.DetectAreaWithSphere(this.transform, CheckPizzaViewRadius, m_playerDetectedColliders, m_layerToCheckPizza) != null)
        {
            pizza = m_playerDetectInterface.DetectAreaWithSphere(this.transform, CheckPizzaViewRadius, m_playerDetectedColliders, m_layerToCheckPizza);

            m_pizzaRef = pizza.GetComponent<PizzaActions>();
            m_pizzaRef.TakePizza(this.transform);
        }
    }

    

    #region DetectionGizmo
    //Gizmo too see the OverlapSphere

#if UNITY_EDITOR

    public void OnDrawGizmosSelected()
    {
        EditorGizmo(transform);

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, CheckPizzaViewRadius);

    }

    public void EditorGizmo(Transform transform)
    {
        ViewGizmo(Color.green, m_detectDeliveryAngle, m_detectDeliveryRadius);
    }

    private void ViewGizmo(Color color, float angle, float radius)
    {
        Color c = color;
        UnityEditor.Handles.color = c;

        Vector3 rotatedForward = Quaternion.Euler(0, -angle * 0.5f, 0) * transform.forward;

        UnityEditor.Handles.DrawSolidArc(transform.position, Vector3.up, rotatedForward, angle, radius);
    }

    
#endif

    #endregion
    #endregion
}

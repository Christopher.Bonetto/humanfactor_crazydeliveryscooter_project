﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour
{
    public Vector3 currentRotation;
    public Vector3 anglesToRotate;

    void Start()
    {
        //currentRotation = new Vector3(currentRotation.x % 360f, currentRotation.y % 360f, currentRotation.z % 360f);
        //anglesToRotate = new Vector3(anglesToRotate.x % 360f, anglesToRotate.y % 360f, anglesToRotate.z % 360f);

        //Quaternion rotationY = Quaternion.AngleAxis(currentRotation.y, new Vector3(0f, 1f, 0f));
        //Quaternion rotationX = Quaternion.AngleAxis(currentRotation.x, new Vector3(1f, 0f, 0f));
        //Quaternion rotationZ = Quaternion.AngleAxis(currentRotation.z, new Vector3(0f, 0f, 1f));
        //this.transform.rotation = rotationY * rotationX * rotationZ;

    }

    void Update()
    {

        Quaternion rotationY = Quaternion.AngleAxis(Input.GetAxis("Horizontal") * 10, new Vector3(0f, 1f, 0f));
        Quaternion rotationX = Quaternion.AngleAxis(Input.GetAxis("Vertical") * 10, new Vector3(1f, 0f, 0f));
        Quaternion rotationZ = Quaternion.AngleAxis(anglesToRotate.z * Time.deltaTime, new Vector3(0f, 0f, 1f));
        this.transform.rotation = this.transform.rotation * rotationY * rotationX * rotationZ;

        currentRotation = currentRotation + anglesToRotate * Time.deltaTime;
        currentRotation = new Vector3(currentRotation.x % 360f, currentRotation.y % 360f, currentRotation.z % 360f);
    }
}

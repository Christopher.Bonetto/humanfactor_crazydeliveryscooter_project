﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PizzaActions : MonoBehaviour
{
    private Transform m_pizzaFather;
    private Rigidbody m_pizzaRb;

    private void Awake()
    {
        m_pizzaRb = gameObject.GetComponent<Rigidbody>();
    }
    private void Start()
    {
        m_pizzaFather = transform.parent;
        SetRigidBody(false);
    }
    

    public void DeliveryThisPizza()
    {
        transform.SetParent(m_pizzaFather);
        GameManager.Instance.ActiveNextGoal();
    }

    public void DropPizza()
    {
        transform.SetParent(m_pizzaFather);
        transform.localScale = new Vector3(2f, 0.2f, 2f);
        SetRigidBody(false);
    }

    public void TakePizza(Transform newParent)
    {
        SetRigidBody(true);

        transform.SetParent(newParent);
        transform.localScale = new Vector3(0.5f, 0.2f, 0.5f);
        transform.localPosition = new Vector3(-0.8f, 1.25f, 0);
        transform.localRotation = Quaternion.EulerAngles(0, 0, 0);
    }

    public void SetRigidBody(bool isKinematic)
    {
        m_pizzaRb.useGravity = !isKinematic;
        m_pizzaRb.detectCollisions = !isKinematic;
        m_pizzaRb.isKinematic = isKinematic;
    }
}

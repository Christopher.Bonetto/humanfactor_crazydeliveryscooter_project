﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public interface ICanDetect
{
    GameObject DetectAreaWithAngle(Transform startDetectPoint, float ViewRadius, float ViewAngle, Collider[] detectedColliders, int numberOfCollidersDetected, LayerMask interestedLayerMask);
    GameObject DetectAreaWithSphere(Transform startDetectPoint, float ViewRadius, Collider[] detectColliders, LayerMask interestedLayerMask , int numberOfCollidersDetected = 1);
}
    


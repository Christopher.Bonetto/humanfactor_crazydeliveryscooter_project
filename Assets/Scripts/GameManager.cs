﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum GameStates
{
    Start,
    Running,
    Pause,
    Lose,
    Victory
}

public class GameManager : MonoBehaviour
{
    public GameStates MG_CurrentGameStates { get; private set; } = GameStates.Running;

    public static GameManager Instance;

    private int Points = 0;

    [SerializeField] private GameObject m_goalsContainer;
    private int m_countChildrens;

    public List<Transform> goals = new List<Transform>();

    private int m_goalCounter = 0;

    private float m_timer = 0;

    private void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        CountGoals();

        ActiveNextGoal();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddPoints(int addPoints)
    {
        Points += addPoints;
        Points = Mathf.Clamp(Points, 0, 1000);
        Debug.Log(Points);
    }

    private void CountGoals()
    {
        m_countChildrens = m_goalsContainer.transform.childCount -1;

        for(int i = 0; i <= m_countChildrens; i++)
        {
            goals.Add(m_goalsContainer.transform.GetChild(i));
            m_goalsContainer.transform.GetChild(i).gameObject.SetActive(false);
        }
    }

    public void ActiveNextGoal()
    {
        if (m_goalCounter < m_countChildrens)
        {
            if (goals[m_goalCounter].gameObject.active)
            {
                goals[m_goalCounter].gameObject.SetActive(false);
                

                m_goalCounter++;
                m_goalCounter = Mathf.Clamp(m_goalCounter, 0, m_countChildrens);


                
                goals[m_goalCounter].gameObject.SetActive(true);
            }
            else
            {
                
                goals[m_goalCounter].gameObject.SetActive(true);
            }
        }
        else
        {
            
            goals[m_goalCounter].gameObject.SetActive(false);
            Debug.Log("finish");
        }
            

    }

    public void SetManagerState(GameStates newState)
    {
        MG_CurrentGameStates = newState;
    }

    public void LoseCondition()
    {
        UIManager.Instance.ShowPanel(true);
    }

    public void RestartScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public bool Timer(float destinationTime)
    {
        m_timer += Time.deltaTime;

        if (m_timer >= destinationTime)
        {
            m_timer = 0;
            return true;
        }
        else
        {
            return false;
        }
    }
}

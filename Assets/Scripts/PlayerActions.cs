﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.AI;
//using UnityEngine.SceneManagement;

//public class PlayerActions : MonoBehaviour
//{
//    private NavMeshAgent m_playerAgent;
//    private Vector3 m_playerMovement;

//    #region Detection Area

//    public ICanDetect m_playerDetectInterface { get; private set; }

//    [Range(0f,130f),SerializeField]
//    private float m_playerViewAngle;

//    public float CurrentAngle
//    {
//        get
//        {
//            return Mathf.Clamp(m_playerViewAngle, 0, 130f);
//        }
//        private set
//        {
//            m_playerViewAngle = value;
//        }
//    }

//    [Range(0f,2f),SerializeField]
//    private float m_playerViewRadius;

//    public float CurrentRadius
//    {
//        get
//        {
//            return Mathf.Clamp(m_playerViewAngle, 0, 2f);
//        }
//        private set
//        {
//            m_playerViewAngle = value;
//        }
//    }

//    [SerializeField]
//    private LayerMask m_detectionMaskForAuto;


//    [SerializeField]
//    private LayerMask m_detectionMaskForTresh;
    
//    private Collider[] m_playerDetectedColliders = new Collider[1];
//    private int m_numberOfColliderDetected;

//    #endregion

//    private GameObject m_currentCar = null;

//    private GameObject m_currentTresh = null;
//    private TreshActions m_treshRef = null;


//    private void Awake()
//    {
//        m_playerAgent = gameObject.GetComponent<NavMeshAgent>();
//    }

//    // Start is called before the first frame update
//    void Start()
//    {
//        m_playerAgent.updateRotation = false;

//        m_playerDetectInterface = new DetectionAreaNoBehaviour();
//    }

//    // Update is called once per frame
//    void Update()
//    {
//        Move();

//        if (Input.GetKeyDown(KeyCode.E))
//        {
//            if (m_currentTresh == null)
//            {
//                TakeTresh();
//            }
//            else
//            {
//                ReleaseTresh();
//            }
//        }

//        if (m_currentCar == null)
//        {
//            TakeAuto();
//        }
//    }
    
//    //Move the player taken GetAxis values
//    private void Move()
//    {
//        m_playerMovement = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized;
//        m_playerAgent.Move(m_playerMovement * m_playerAgent.speed * Time.deltaTime);

//        if (m_playerMovement != Vector3.zero)
//        {
//            transform.forward = m_playerMovement;
//        }
//    }

//    //Called to stop the player.
//    public void StopPlayer()
//    {
//        m_playerMovement = Vector3.zero;
//    }

//    private void TakeAuto()
//    {
//        if (Input.GetKeyDown(KeyCode.Q))
//        {
//            if (m_playerDetectInterface.DetectArea(gameObject.transform, m_playerViewRadius, m_playerViewAngle, m_playerDetectedColliders, m_numberOfColliderDetected, m_detectionMaskForAuto) != null)
//            {
//                GameObject tempCar;

//                tempCar = m_playerDetectInterface.DetectArea(gameObject.transform, m_playerViewRadius, m_playerViewAngle, m_playerDetectedColliders, m_numberOfColliderDetected, m_detectionMaskForAuto);

//                CarsAction tempCarRef = tempCar.GetComponent<CarsAction>();

//                if (tempCarRef != null)
//                {
//                    m_currentCar = tempCar;

//                    transform.SetParent(m_currentCar.transform);
//                    transform.localPosition = new Vector3(0, 0, 0);
//                    transform.forward = m_currentCar.transform.forward;

//                    tempCarRef.m_playerInCarRef = this;
//                    tempCarRef.m_playerIsInCar = true;

//                    gameObject.SetActive(false);
//                }
//            }
//        }
//    }

//    public void TakeTresh()
//    {
//        if (m_playerDetectInterface.DetectArea(gameObject.transform, m_playerViewRadius, m_playerViewAngle, m_playerDetectedColliders, m_numberOfColliderDetected, m_detectionMaskForTresh))
//        {
//            GameObject tempTresh;

//            tempTresh = m_playerDetectInterface.DetectArea(gameObject.transform, m_playerViewRadius, m_playerViewAngle, m_playerDetectedColliders, m_numberOfColliderDetected, m_detectionMaskForTresh);

//            m_treshRef = tempTresh.GetComponent<TreshActions>();


//            if (m_treshRef != null)
//            {
//                m_currentTresh = tempTresh;

//                m_currentTresh.transform.SetParent(transform);
                
//                m_currentTresh.transform.localPosition = new Vector3(0, -0.5f, 1.5f);

//                m_treshRef.isGrabbed = true;
//                m_treshRef.playerRef = this;
                
//            }
//        }
//    }

//    public void ReleaseTresh()
//    {
//        m_treshRef.TreshReleased();
        
//        m_currentTresh = null;
//    }

//    public void ActiveAndReleasePlayer()
//    {
//        m_currentCar = null;
//        transform.parent = null;
//    }
    

//    #region DetectionGizmo
//    //Gizmo too see the OverlapSphere

//#if UNITY_EDITOR

//    public void OnDrawGizmosSelected()
//    {
//        EditorGizmo(transform);
//    }

//    public void EditorGizmo(Transform transform)
//    {
//        ViewGizmo(Color.green, m_playerViewAngle, m_playerViewRadius);
//    }

//    private void ViewGizmo(Color color, float angle, float radius)
//    {
//        Color c = color;
//        UnityEditor.Handles.color = c;

//        Vector3 rotatedForward = Quaternion.Euler(0, -angle * 0.5f, 0) * transform.forward;

//        UnityEditor.Handles.DrawSolidArc(transform.position, Vector3.up, rotatedForward, angle, radius);
//    }
//#endif

//    #endregion
//}
